
getCurrentTabId(function(tabId){
    //获取当前页面地址
    chrome.tabs.executeScript(tabId,{
        "code":'window.location.href'
    },function(output){
        var url = (output && output.length>0)?output[0]:null;
        if(url != null){
            //使用当前地址生成二维码
            new QRCode(document.getElementById("qrcode"), {
                text: url,
                width: 200,
                height: 200,
                colorDark : "#000000",
                colorLight : "#ffffff",
                correctLevel : QRCode.CorrectLevel.H
            });
        }
    })
});