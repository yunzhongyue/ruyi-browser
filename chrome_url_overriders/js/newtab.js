var options_key = "RUYI_BROWSER_OPTIONS"; //配置数据存储key

//获取当前配置信息
chrome.storage.local.get(options_key,function(data){
    if(data[options_key]){
        var config_data = data[options_key]['site_nav'];
        var site_nav_html = '<dl>';
        for(var i=0;i<config_data.length;i++){
            var classify = config_data[i];
            var classify_name = classify.name;
            var sites = classify.site;
            site_nav_html += '<dt>'+classify_name+'</dt>'
            site_nav_html += '<dd>';
            for(var n=0;n<sites.length;n++){
                site_nav_html += '<li><a href="'+sites[n].url+'">'+sites[n].name+'</a></li>';
            }
            site_nav_html += '</dd>';
        }
        site_nav_html += '</dl>';
        document.getElementById("main").innerHTML = site_nav_html;
    }
});