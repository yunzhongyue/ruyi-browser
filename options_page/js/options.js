var default_config = {
    "site_nav":[
        {
            "name":"常用",
            "keyword":"cy",
            "site":[
                {
                    "name":"百度",
                    "url":"https://www.baidu.com"
                },
                {
                    "name":"阿里云大学",
                    "url":"https://edu.aliyun.com/"
                }
            ]
        },
        {
            "name":"工具",
            "keyword":"gj",
            "site":[
                {
                    "name":"雨露网-在线工具",
                    "url":"https://www.yu-lu.cn/tools"
                },
                {
                    "name":"开源中国-在线工具",
                    "url":"https://tool.oschina.net/"
                }
            ]
        },
        {
            "name":"技术",
            "keyword":"js",
            "site":[
                {
                    "name":"开源中国",
                    "url":"https://www.oschina.net/"
                },
                {
                    "name":"码云",
                    "url":"https://gitee.com/"
                },
                {
                    "name":"GitHub",
                    "url":"https://github.com/"
                }
            ]
        }
    ]
};

var options_key = "RUYI_BROWSER_OPTIONS"; //配置数据存储key
var config_textarea = document.getElementById("config");

//获取当前配置信息
chrome.storage.local.get(options_key,function(data){
    if(!data[options_key]){
        data[options_key] = default_config;
        chrome.storage.local.set(data,function(){
            console.log("store data successed!!!")
        });
    }
    config_textarea.value = JSON.stringify(data[options_key],null,2); //初始化配置选项文本框
});

config_textarea.addEventListener('input',function(){
    var data = {};
    data[options_key] = JSON.parse(config_textarea.value);
    chrome.storage.local.set(data,function(){
        console.log("store data successed!!!")
    });
});